package com.example.calculatoradvanced;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {
    TextView hasil;
    Button kembali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        kembali = findViewById(R.id.back);
        kembali.setOnClickListener(this);

        hasil = findViewById(R.id.textView);

        Intent in = getIntent();
        Bundle b = in.getExtras();
        String h = b.getString("saveResult");

        hasil.setText(h);
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}

